#![warn(clippy::pedantic)]

use criterion::{black_box, criterion_group, criterion_main, Criterion};

use projecteuler::p67;

pub fn pe_benchmarks(c: &mut Criterion) {
    c.bench_function("p67", |b| b.iter(|| black_box(p67::solve())));
}

criterion_group!(benches, pe_benchmarks);
criterion_main!(benches);
