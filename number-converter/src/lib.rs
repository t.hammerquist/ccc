use lazy_static::lazy_static;

const NITS: &str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
lazy_static! {
    pub static ref VNITS: Vec<char> = NITS.chars().collect();
}

#[must_use]
pub fn int_to_str(n: u64, b: u8) -> Option<String> {
    dbg!(&n, &b);
    let mut chars = vec![];
    let mut n = n;
    let b: u64 = b.into();

    while n > 0 {
        if let Some(c) = VNITS.get((n % b) as usize) {
            chars.push(*c);
            n /= b;
        } else {
            return None;
        }
    }

    if chars.is_empty() {
        chars.push('0');
    } else {
        chars.reverse();
    }

    Some(chars.iter().collect())
}

#[must_use]
pub fn str_to_int(s: &str, b: u8) -> Option<u64> {
    let mut result = 0;
    let mut mag = 1;
    let b: u64 = b.into();

    for c in s.chars().rev() {
        if let Some(n) = NITS.find(c) {
            let n = n as u64;

            if n > b {
                return None;
            }

            result += n * mag;
            if let Some(prod) = mag.checked_mul(b) {
                mag = prod;
            }
        } else {
            return None;
        }
    }

    Some(result)
}

#[cfg(test)]
mod tests {
    use rand::{thread_rng, RngCore};

    use super::*;

    #[test]
    fn test_i2s() {
        assert_eq!(Some("1010".into()), int_to_str(10, 2));
        assert_eq!(Some("A".into()), int_to_str(10, 16));
        assert_eq!(Some("102".into()), int_to_str(123, 11));
        assert_eq!(Some("1A2".into()), int_to_str(233, 11));
    }

    #[test]
    fn test_s2i() {
        assert_eq!(Some(10), str_to_int("1010", 2));
        assert_eq!(Some(10), str_to_int("A", 16));
        assert_eq!(Some(123), str_to_int("102", 11));
        assert_eq!(Some(233), str_to_int("1A2", 11));
    }

    #[test]
    fn test_zero() {
        assert_eq!(Some("0".into()), int_to_str(0, 2));
        assert_eq!(Some("0".into()), int_to_str(0, 36));
        assert_eq!(Some(0), str_to_int("0", 9));
        assert_eq!(Some(0), str_to_int("00000", 25));
    }

    #[test]
    fn test_max_u64() {
        assert_eq!(Some("FFFFFFFFFFFFFFFF".into()), int_to_str(u64::MAX, 16));
        assert_eq!(Some(u64::MAX), str_to_int("FFFFFFFFFFFFFFFF", 16));
    }

    #[test]
    fn test_fuzz_i2s() {
        let mut rng = thread_rng();
        for _ in 0..1024 {
            let n = rng.next_u64();
            assert!(int_to_str(n, 16).is_some());
        }
    }

    #[test]
    fn test_fuzz_s2i() {
        let mut rng = thread_rng();
        for _ in 0..100 {
            let n = rng.next_u64();
            assert!(int_to_str(n, 16).is_some());
        }
    }

    #[test]
    fn test_invalid_inputs_s2i() {
        for (s, b) in [("A%X", 36), ("I", 16), ("100K", 2)] {
            let opt_n = str_to_int(s, b);
            dbg!(&s, &b, &opt_n);
            assert!(opt_n.is_none());
        }
    }
}
