# Number Converter

## Overview

In this assignment, we will be converting integers to and from arbitrary based
representation. What does this mean? Well, our normal counting system is
base-10, or decimal. Some other common ones you may have seen are base-2
(binary), and base-16 (hexadecimal). Well, if we can have base 2, base 10, and
base 16, why can’t we have arbitrary bases? Well it turns out we can! Using
just alphanumeric characters, we can have up to base 36 representation trivially
(10 digits 0-9, and 26 letters A-Z).

So for this assignment, we will be making a program that can convert to or
from numeric representations. To do so, we will want to get refreshed on two
important operations: integer division, and the modulus operator.

### Integer division

> As a refresher for some, and guidance for others, integer division (dividing
> one integer by another) in many programming languages (but not Python) is
> treated differently than in your grade-school arithmetic. In school, your math
> may have worked out such that 5/2=2.5, and in all other parts of your life
> this is perfectly valid. However, integer division works differently. In this
> case, 5/2=2. The general form of this is that in integer division, we round
> down to the closest whole number. We never round up. So, 6/2=3, 6/3=2, and
> 6/4=1.

### Modulus operator

> The modulus operator performs division like above, but instead of returning
> the lowest whole number, it returns the remainder of the division operation.
> So in school you learned that 6/4=1 remainder 2. We just learned that integer
> division yields us 6/4=1. The modulus operator, %, yields us 6%4=2, because
> the remainder of dividing 6 by 4 is 2.

These two operators will give us the mathematical operations we need to complete the assignment.

## Assignment specifics

You may choose any common programming language to complete this assignment.
Please do not select a custom-made programming language, or any esoteric
programming language like Brainf*ck. This is meant to be an exercise in
programming, not an exercise in headaches!

At minimum, this assignment will require writing two functions:

- `convertIntToString(num: int, base: int) -> str`
- `convertStringToInt(num: str, base: int) -> int`

where base in both functions refers to the numeric counting base that we wish to
convert to/from. base must be between 2 and 36, inclusive.

### Example

When your functions are called with the following parameters, we might expect to
see the following outputs:

#### `convertIntToString(num: int, base: int) -> str`

| num   | base   | result   |
| ---   | ---    | ---      |
| `10`  | `2`    | `"1010"` |
| `10`  | `16`   | `"A"`    |
| `123` | `11`   | `"102"`  |
| `233` | `11`   | `"1A2"`  |

#### `convertStringToInt(num: str, base: int) -> int`

| num      | base   | result   |
| ---      | ---    | ---      |
| `"1010"` | `2`    | `10`     |
| `"A"`    | `16`   | `10`     |
| `"102"`  | `11`   | `123`    |
| `"1A2"`  | `11`   | `233`    |
