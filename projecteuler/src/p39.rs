//! Problem 39: Integer Right Triangles
//!
//! <https://projecteuler.net/problem=39>
//!
//! If is the perimeter of a right angle triangle with integral length sides, `{a, b, c}`, there
//! are exactly three solutions for `p = 120`:
//!
//! ```ignore
//! {20, 48, 52}
//! {24, 45, 51}
//! {30, 40, 50}
//! ```
//!
//! For which value of `p <= 1000` is the number of solutions maximised?

fn solutions_for_p(p: i32) -> i32 {
    let mut total = 0;

    for c in 2..(p / 2) {
        for b in 2..c {
            let a = p - c - b;

            if a >= b && c.pow(2) == a.pow(2) + b.pow(2) {
                total += 1;
            }
        }
    }

    total
}

/// # Panics
#[allow(dead_code)]
#[must_use]
pub fn solve() -> i32 {
    (3..=1_000)
        .map(|p| (p, solutions_for_p(p)))
        .max_by_key(|tup| tup.1)
        .expect("need at least one number in range")
        .0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        assert_eq!(3, solutions_for_p(120));
    }

    #[test]
    fn test_question() {
        assert_eq!(840, solve());
    }
}
