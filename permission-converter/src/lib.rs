#![warn(clippy::pedantic)]

#[derive(Clone, Copy, Debug)]
enum Permissions {
    OX = 1,
    OW = 2,
    OR = 4,
    GX = 8,
    GW = 16,
    GR = 32,
    UX = 64,
    UW = 128,
    UR = 256,
    D = 512,
}

impl Permissions {
    const ALL: [Permissions; 10] = [
        Permissions::D,
        Permissions::UR,
        Permissions::UW,
        Permissions::UX,
        Permissions::GR,
        Permissions::GW,
        Permissions::GX,
        Permissions::OR,
        Permissions::OW,
        Permissions::OX,
    ];

    // integer is used in tests, and may be used by consumers
    #[allow(dead_code)]
    const FULL: u16 = 0b11_1111_1111;

    fn to_char(self) -> char {
        match self {
            Permissions::OX | Permissions::GX | Permissions::UX => 'x',
            Permissions::OW | Permissions::GW | Permissions::UW => 'w',
            Permissions::OR | Permissions::GR | Permissions::UR => 'r',
            Permissions::D => 'd',
        }
    }

    fn as_u16(self) -> u16 {
        self as u16
    }
}

#[must_use]
pub fn str2int(s: &str) -> Option<u16> {
    if s.len() != 10 {
        eprintln!("Invalid length {}: {s}", s.len());
        return None;
    }

    let mut sum = 0;
    for (c, p) in s.chars().zip(Permissions::ALL) {
        match c {
            '-' => (),
            x if x == p.to_char() => {
                sum |= p.as_u16();
            }
            _ => {
                eprintln!("Invalid char {c} for perm {p:?}");
                return None;
            }
        }
    }

    Some(sum)
}

#[must_use]
pub fn int2str(n: u16) -> String {
    Permissions::ALL
        .iter()
        .map(|perm| match n & perm.as_u16() {
            0 => '-',
            _ => perm.to_char(),
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_str2int_empty() {
        assert_eq!(0, str2int("----------").unwrap());
    }

    #[test]
    fn test_str2int_full() {
        assert_eq!(Permissions::FULL, str2int("drwxrwxrwx").unwrap());
    }

    #[test]
    fn test_str2int_only_dir() {
        assert_eq!(Permissions::D.as_u16(), str2int("d---------").unwrap());
    }

    #[test]
    fn test_str2int_only_user() {
        assert_eq!(
            Permissions::UR.as_u16() | Permissions::UW.as_u16() | Permissions::UX.as_u16(),
            str2int("-rwx------").unwrap()
        );
    }

    #[test]
    fn test_str2int_only_group() {
        assert_eq!(
            Permissions::GR.as_u16() | Permissions::GW.as_u16() | Permissions::GX.as_u16(),
            str2int("----rwx---").unwrap()
        );
    }

    #[test]
    fn test_str2int_only_other() {
        assert_eq!(
            Permissions::OR.as_u16() | Permissions::OW.as_u16() | Permissions::OX.as_u16(),
            str2int("-------rwx").unwrap()
        );
    }

    #[test]
    fn test_str2int_offset_invalid_length() {
        assert!(str2int("------rwx").is_none());
    }

    #[test]
    fn test_str2int_offset_perms_fail() {
        assert!(str2int("------rwx-").is_none());
    }

    #[test]
    fn test_str2int_invalid_chars() {
        for s in ["r---------", "-d--------", "--r-------", "--------xw"] {
            assert!(str2int(s).is_none());
        }
    }

    #[test]
    fn test_int2str_empty() {
        assert_eq!("----------", int2str(0));
    }

    #[test]
    fn test_int2str_full() {
        assert_eq!("drwxrwxrwx", int2str(Permissions::FULL));
    }

    #[test]
    fn test_int2str_max() {
        assert_eq!("drwxrwxrwx", int2str(u16::MAX));
    }
}
