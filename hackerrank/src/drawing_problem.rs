//! <https://www.hackerrank.com/challenges/drawing-book/problem>
//!
//! Complete the 'pageCount' function below.
//! The function is expected to return an INTEGER.
//! The function accepts following parameters:
//!  1. INTEGER n
//!  2. INTEGER p

#[allow(dead_code, non_snake_case)]
fn pageCountAlt(n: i32, p: i32) -> i32 {
    let total = if n % 2 == 0 { n + 1 } else { n };
    let front = p / 2;
    let back = (total - p) / 2;
    dbg!((&n, &p, &total, &front, &back));
    std::cmp::min(front, back)
}

#[allow(dead_code, non_snake_case)]
fn pageCount(n: i32, p: i32) -> i32 {
    dbg!(&n, &p);
    if p > n / 2 {
        let n = if n % 2 == 0 { n + 1 } else { n };
        (n - p) / 2
    } else {
        p / 2
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_CASES: &[(i32, i32, i32)] = &[
        (8, 1, 0),
        (8, 2, 1),
        (8, 3, 1),
        (8, 4, 2),
        (8, 5, 2),
        (8, 6, 1),
        (8, 7, 1),
        (8, 8, 0),
        (9, 1, 0),
        (9, 2, 1),
        (9, 3, 1),
        (9, 4, 2),
        (9, 5, 2),
        (9, 6, 1),
        (9, 7, 1),
        (9, 8, 0),
        (9, 9, 0),
    ];

    #[test]
    fn test_page_count_alt() {
        for (n, p, r) in TEST_CASES {
            assert_eq!(*r, pageCountAlt(*n, *p));
        }
    }

    #[test]
    fn test_page_count() {
        for (n, p, r) in TEST_CASES {
            assert_eq!(*r, pageCount(*n, *p));
        }
    }
}
