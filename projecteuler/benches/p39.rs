#![warn(clippy::pedantic)]

use criterion::{black_box, criterion_group, criterion_main, Criterion};

use projecteuler::p39;

pub fn pe_benchmarks(c: &mut Criterion) {
    c.bench_function("p39", |b| b.iter(|| black_box(p39::solve())));
}

criterion_group!(benches, pe_benchmarks);
criterion_main!(benches);
