//! Problem 36: Double-base Palindromes
//!
//! <https://projecteuler.net/problem=36>
//!
//! The decimal number `585` (`1001001001` binary), is palindromic in both bases.
//!
//! Find the sum of all numbers, less than one million, which are palindromic
//! in base 10 and base 2.
//!
//! (Please note that the palindromic number, in either base, may not include leading zeros.)

fn is_palindrome(n: i32, b: i32) -> bool {
    let mut dits = vec![];
    let mut n = n;
    while n > 0 {
        dits.push(n % b);
        n /= b;
    }
    // dbg!(&dits);

    let last_i = dits.len() - 1;
    for i in 0..=last_i / 2 {
        // dbg!(&i, &dits[i], &dits[last_i - i]);
        if dits[i] != dits[last_i - i] {
            return false;
        }
    }

    true
}

/// # Panics
#[allow(dead_code)]
#[must_use]
pub fn solve() -> i32 {
    (1..=1_000_000i32)
        .step_by(2)
        .filter(|&n| is_palindrome(n, 2) && is_palindrome(n, 10))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_not_palindrome_b2() {
        assert!(!is_palindrome(0b10, 2));
        assert!(!is_palindrome(0b100, 2));
        assert!(!is_palindrome(0b110, 2));
        assert!(!is_palindrome(0b101010111, 2));
        assert!(!is_palindrome(0b111010101, 2));
    }

    #[test]
    fn test_palindrome_b2() {
        assert!(is_palindrome(0b1, 2));
        assert!(is_palindrome(0b11, 2));
        assert!(is_palindrome(0b101, 2));
        assert!(is_palindrome(0b111, 2));
        assert!(is_palindrome(0b1001, 2));
        assert!(is_palindrome(0b1111, 2));
        assert!(is_palindrome(0b101010101, 2));
        assert!(is_palindrome(0b111010111, 2));
    }

    #[test]
    fn test_not_palindrome_b10() {
        assert!(!is_palindrome(10, 10));
        assert!(!is_palindrome(100, 10));
        assert!(!is_palindrome(110, 10));
        assert!(!is_palindrome(153, 10));
        assert!(!is_palindrome(16562, 10));
        assert!(!is_palindrome(12345432, 10));
    }

    #[test]
    fn test_palindrome_b10() {
        assert!(is_palindrome(1, 10));
        assert!(is_palindrome(11, 10));
        assert!(is_palindrome(101, 10));
        assert!(is_palindrome(111, 10));
        assert!(is_palindrome(1001, 10));
        assert!(is_palindrome(1111, 10));
        assert!(is_palindrome(101010101, 10));
        assert!(is_palindrome(111010111, 10));
    }

    #[test]
    fn test_answer() {
        assert_eq!(872_187, solve());
    }
}
