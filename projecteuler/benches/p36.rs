#![warn(clippy::pedantic)]

use criterion::{black_box, criterion_group, criterion_main, Criterion};

use projecteuler::p36;

pub fn pe_benchmarks(c: &mut Criterion) {
    c.bench_function("p36", |b| b.iter(|| black_box(p36::solve())));
}

criterion_group!(benches, pe_benchmarks);
criterion_main!(benches);
