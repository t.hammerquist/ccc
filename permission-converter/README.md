# Permission Converter

## Overview

In this assignment, we will convert permission strings to their numeric representation, and vice versa. What does this mean? Well, perhaps you have seen
permissions on files represented as something to the effect of the following:

```
-rwxr-xr-x 1 user staff 817 Mar 24 09:45 myfile
```

We are interested in the first piece of that: `-rwxr-xr-x`.

For those that are not familiar, this indicates the following for file `myfile`:

- The user user who owns the file can read from, write to, and execute the file.
- The permission group staff can read from and execute the file.
- Any user on the system can read from and execute the file.

This is because a permission string can be broken up into three parts:

| User             | Group             | Other             |
| ---------------- | ----------------- | ----------------- |
| user permissions | group permissions | other permissions |

Each of those three groups can be broken into the following parts:

| read             | write             | execute             |
| ---------------- | ----------------- | ------------------- |
| read permissions | write permissions | execute permissions |

Each of these fields can be either 1 (`true`) or 0 (`false`). So, it follows
that they can be represented by a single bit.

If we say that `u` stands for `user`, `g` for `group`, and `o` for `other`, and `r` stands for
read permission, `w` for write permission, and `x` for execute permission,
we can represent the full set of permissions `-rwxr-xr-x` on a file as follows:

| ur | uw | ux | gr | gw | gx | or | ow | ox |
| -- | -- | -- | -- | -- | -- | -- | -- | -- |
| 1  | 1  | 1  | 1  | 0  | 1  | 1  | 0  | 1  |

This, as you may have noticed, is just binary! So the binary string representing
this would be `111101101`. This can also be represented by octal (base 8) digits
`755`. More specifically, since 9 bits doesn’t align nicely to anything, it is often
represented as `000111101101` or `0755`.
But what is that extra dash? Well, typically this represents whether or not a
file is a directory. So, for a directory you might see something like the following:

```
drwxr-xr-x 7 user staff 224 Mar 29 11:10 mydirectory
```

Now, for this assignment, this introduction to permissions is sufficient.
However, it should be noted that you may occasionally come across some
variations in the string having to do with setuid and other things. For
simplicitiy’s sake, we are ignoring this.

Another thing we may find useful for this assignment is bitwise arithmetic.
Specifically, we are interested in the following binary operations:

- AND
- OR
- bit shifts

As a refresher, bitwise operations `AND` and `OR` operate on single bits as
boolean values. Here’s a quick truth table for each:

### AND

| bit 1 | bit 2 | result | 
| ----- | ----- | ------ |
| 0     | 0     | 0      |
| 0     | 1     | 0      |
| 1     | 0     | 0      | 
| 1     | 1     | 1      |

### OR

| bit 1 | bit 2 | result |
| ----- | ----- | ------ |
| 0     | 0     | 0      |
| 0     | 1     | 1      |
| 1     | 0     | 1      |
| 1     | 1     | 1      |


So, let’s look at some simple examples of arithmetic on binary numbers:

```
    101001010110
AND 101010101010
----------------

    101000000010
    101001010110
 OR 101010101010
 ---------------
    101011111110
```

Bit shifting is the idea of shifting bits left or right in a number.

If we have a byte represented in binary `00001110` and we shift it to the left
by 1, it will look like `00011100`. If we were to shift the original number to
the right, it would look like `00000111`. For most programming languages, if a
bit shifts beyond the bounds of the data type, those bits just "fall off" the
end and disappear. So, if we shifted `10000001` to the left, we would have
`00000010`.

## Objective

Our objective for this assignment is to be able to convert permission strings
to numeric representations, and numeric representations to permission strings.
This can be achieved by parsing a single character from the string at a time,
shifting our result, and `OR`ing a 1 bit into our result.

For example, suppose we have the following permission string: `-rwxr-xr-x`. Let
us work through a few steps of what the calculation might look like.

```
Initial state
result = 000000000000
perm_str = "-rwxr-xr-x"
cur_char = None

1.
cur_char = '-'
perm_string = "rwxr-xr-x"
result = 000000000000

2.
cur_char = 'r'
perm_string = "wxr-xr-x"
result = 000000000001

3.
cur_char = 'w'
perm_string = "xr-xr-x"
result = 000000000011

4.
cur_char = 'x'
perm_string = "r-xr-x"
result = 000000000111

5.
cur_char = 'r'
perm_string = "-xr-x"
result = 000000001111

6.
cur_char = '-'
perm_string = "xr-x"
result = 000000011110
7.
cur_char = 'x'
perm_string = "r-x"
result = 000000111101
...
```

As you can see, we are building up the permission string as we go.

This process can be reversed to convert from an integer to the permission string.
This can be a little more difficult because you must also keep track of what
permission type you are on (read, write, or execute). I leave it to the reader to
demonstrate for themselves how this might work.

## Assignment specifics

You may choose any common programming language to complete this assignment.
Please do not select a custom-made programming language, or any esoteric
programming language like Brainf*ck. This is meant to be an exercise in
programming, not an exercise in headaches!

At the minimum, your program should have two functions: One for converting from
a permission string to an integer, and one to convert from an integer to a
permission string. These can be called directly with input literals from a main
method for simplicity if so desired. This is not supposed to be stressful, or an
exercise in argument parsing.

Some add-ons for extra credit (+20% of your grade each) might be:

- Make it a commandline-driven program that takes arguments.
- Make it automatically detect whether you are passing a permission string or an integer
- … make something up!
